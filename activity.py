from abc import ABC, abstractclassmethod 
class Animal(ABC):
	@abstractclassmethod
	def eat(self, food):
		pass;
	@abstractclassmethod
	def make_sound(self):
		pass;

class Cat(Animal):
	def __init__(self, name, breed, age):
		super().__init__()
		self._name = name;
		self._breed = breed;
		self._age = age;

	def eat(self,food):
		print (f"Serve me {food}");

	def make_sound(self):
		print (f"Meow, Meow");

	def call(self):
		print(f"{self._name}, come on!")

	def set_name(self,name):
		self._name = name;

	def setter_breed(self,breed):       
		self._breed = breed;

	def setter_age(self,age):        
		self._age = age;

	def getter_name(self):
		return self._name;

	def getter_breed(self):       
		return self._breed;

	def getter_age(self):        
		return self._age;



class Dog(Animal):
	def __init__(self, name, breed, age):
		super().__init__()
		self._name = name;
		self._breed = breed;
		self._age = age;

	def eat(self,food):
		print (f"Eaten {food}");

	def make_sound(self):
		print (f"Aww! Aww!");

	def call(self):
		print(f"Here {self._name}!")


	def setter_name(self,name):
		self._name = name;

	def setter_breed(self,breed):       
		self._breed = breed;

	def setter_age(self,age):        
		self._age = age;

	def getter_name(self):
		return self._name;

	def getter_breed(self):       
		return self._breed;

	def getter_age(self):        
		return self._age;


animal = Dog("Doggy", "Chihuahua", 1);
animal1 = Cat("Catty", "Persian", 2);

animal.eat("Dog Food");
animal.make_sound();
animal.call()

animal1.eat("Cat Food");
animal1.make_sound();
animal1.call()